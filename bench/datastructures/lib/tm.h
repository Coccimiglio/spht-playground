#ifndef TM_H
#define TM_H 1

#  include <stdio.h>

#ifndef REDUCED_TM_API

#  define MAIN(argc, argv)              int main (int argc, char** argv)
#  define MAIN_RETURN(val)              return val

#  define TM_PRINTF                     printf
#  define TM_PRINT0                     printf
#  define TM_PRINT1                     printf
#  define TM_PRINT2                     printf
#  define TM_PRINT3                     printf


#  include <assert.h>
#  include "memory.h"

#  include "thread.h"
#  include <math.h>

#include "spins.h"
#include "impl.h"
#include "htm_impl.h"

#  define TM_ARG                         /* nothing */
#  define TM_ARG_ALONE                   /* nothing */
#  define TM_ARGDECL                     /* nothing */
#  define TM_ARGDECL_ALONE               /* nothing */
#  define TM_CALLABLE                    /* nothing */

// outside the TX
# define S_MALLOC                       nvmalloc
# define S_FREE                         nvfree

# define P_MALLOC(_size)                ({ void *_PTR = nvmalloc_local(HTM_SGL_tid, _size); /*onBeforeWrite(HTM_SGL_tid, _ptr, 0);*/ _PTR; })
# define P_MALLOC_THR(_size, _thr)      ({ void *_PTR = nvmalloc_local(_thr, _size); /*onBeforeWrite(HTM_SGL_tid, _ptr, 0);*/ _PTR; })
# define P_FREE(ptr)                    nvfree(ptr)

// inside the TX
# define TM_MALLOC(_size)               \
({ \
  void *_PTR; \
  _PTR = nvmalloc_local(HTM_SGL_tid, _size); \
  onBeforeWrite(HTM_SGL_tid, _PTR, 0); \
  _PTR; \
})
# define TM_FREE(ptr)                   ({ onBeforeWrite(HTM_SGL_tid, ptr, 0); nvfree(ptr); })

# define SETUP_NUMBER_TASKS(n)
# define SETUP_NUMBER_THREADS(n)
# define PRINT_STATS()
# define AL_LOCK(idx)

#endif

# define TM_STARTUP(numThread) \
  HTM_init(numThread); \
  guy_configSetup(); \
  global_structs_init( \
    numThread, \
    numThread, \
    2 /* NB_EPOCHS: use max of 1073741824 */, \
    268435456L /* LOG_SIZE: in nb of entries */, \
    2281701376L /* LOCAL_MEM_REGION (500M) */, \
    8589934592L /* SHARED_MEM_REGION (1G) */, \
    NVRAM_REGIONS \
  ) \
//

# define TM_SHUTDOWN() { \
  global_structs_destroy(); \
}

# define TM_THREAD_ENTER(tid) \
  HTM_thr_init(tid); \
  HTM_set_is_record(1); \
//

# define TM_THREAD_EXIT() \  
  HTM_thr_exit(); \
//

# define TM_BEGIN() \
  NV_HTM_BEGIN(HTM_SGL_tid) \
//

# define TM_END() \
  NV_HTM_END(HTM_SGL_tid) \
//

# define TM_RESTART()                  HTM_abort();

# define TM_LOCAL_WRITE(var, val)      ({ var = val; var; })
# define TM_LOCAL_WRITE_P(var, val)    ({ var = val; var; })
# define TM_LOCAL_WRITE_D(var, val)    ({ var = val; var; })

# define TM_SHARED_READ(var) (var)
# define TM_SHARED_READ_P(var) (var)
# define TM_SHARED_READ_D(var) (var)
# define TM_SHARED_READ_F(var) (var)
# define TM_SHARED_WRITE(var, val)   ({ onBeforeWrite(HTM_SGL_tid, &var, val); var = val; var;})
# define TM_SHARED_WRITE_P(var, val) ({ onBeforeWrite(HTM_SGL_tid, &var, val); var = val; var;})
# define TM_SHARED_WRITE_D(var, val) ({ onBeforeWrite(HTM_SGL_tid, &var, val); var = val; var;})
# define TM_SHARED_WRITE_F(var, val) ({ onBeforeWrite(HTM_SGL_tid, &var, val); var = val; var;})

// ----------------------------------------------
#define SLOW_PATH_SHARED_READ TM_SHARED_READ
#define FAST_PATH_SHARED_READ TM_SHARED_READ
#define SLOW_PATH_SHARED_READ_D TM_SHARED_READ_D
#define FAST_PATH_SHARED_READ_D TM_SHARED_READ_D
#define SLOW_PATH_SHARED_READ_P TM_SHARED_READ_P
#define FAST_PATH_SHARED_READ_P TM_SHARED_READ_P

#define SLOW_PATH_SHARED_WRITE TM_SHARED_WRITE
#define FAST_PATH_SHARED_WRITE TM_SHARED_WRITE
#define SLOW_PATH_SHARED_WRITE_D TM_SHARED_WRITE_D
#define FAST_PATH_SHARED_WRITE_D TM_SHARED_WRITE_D
#define SLOW_PATH_SHARED_WRITE_P TM_SHARED_WRITE_P
#define FAST_PATH_SHARED_WRITE_P TM_SHARED_WRITE_P
// ----------------------------------------------

#define USE_PCWM2
#define USE_LOG_REPLAY_PARALLEL

static void guy_configSetup()
{  
  log_replay_flags = LOG_REPLAY_FORWARD;

#if defined(USE_PCWM)  
  printf("usePCWM is set\n");
  install_bindings_pcwm();
  wait_commit_fn = wait_commit_pcwm;
  log_replay_flags |= LOG_REPLAY_PHYSICAL_CLOCKS;
#elif defined(USE_PCWM2)
  printf("usePCWM2 is set\n");
  install_bindings_pcwm2();
  wait_commit_fn = wait_commit_pcwm2;
  log_replay_flags |= LOG_REPLAY_PHYSICAL_CLOCKS_SORTED;  
#elif defined(USE_PCWM3)
  printf("usePCWM3 is set\n");
  install_bindings_pcwm3();
  wait_commit_fn = wait_commit_pcwm3;
  log_replay_flags |= LOG_REPLAY_PHYSICAL_CLOCKS_SORTED_BACKWARD;
#endif

#if defined(USE_LOG_REPLAY_BACKWARD) || defined (USE_PCWM3)
  log_replay_flags = LOG_REPLAY_BACKWARD;
  printf("LOG_REPLAY_BACKWARD is set\n");
#endif
  
#if defined(USE_LOG_REPLAY_BUFFER_WBINVD)
  log_replay_flags |= LOG_REPLAY_BUFFER_WBINVD;
  printf("LOG_REPLAY_BUFFER_WBINVD is set\n");
#elif defined(USE_LOG_REPLAY_BUFFER_FLUSHES)
  log_replay_flags |= LOG_REPLAY_BUFFER_FLUSHES;
  printf("LOG_REPLAY_BUFFER_FLUSHES is set\n");
#elif defined (USE_LOG_REPLAY_RANGE_FLUSHES)
  log_replay_flags |= LOG_REPLAY_RANGE_FLUSHES;
  printf("LOG_REPLAY_RANGE_FLUSHES is set\n");
#endif
  
#if defined(USE_LOG_REPLAY_ASYNC_SORTER)
  log_replay_flags |= LOG_REPLAY_ASYNC_SORTER;
  printf("LOG_REPLAY_ASYNC_SORTER is set\n");
#endif  

#if defined(USE_LOG_REPLAY_PARALLEL)
  log_replay_flags |= LOG_REPLAY_PARALLEL;
  printf("LOG_REPLAY_PARALLEL is set\n");
#endif

#if defined(DISABLE_LOG_REPLAY)
  printf("DISABLE_LOG_REPLAY is set\n");
  log_replay_flags = 0;
#endif  
  printf(" --- \n");
}

#endif
