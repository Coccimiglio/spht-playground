#!/bin/bash

FOLDERS="hashmap"

htm_retries=5
rot_retries=2

# if [ $# -eq 3 ] ; then
#     htm_retries=$2 # e.g.: 5
#     rot_retries=$3 # e.g.: 2, this can also be retry policy for tle
# fi

rm lib/*.o || true

CPU_FREQ=$(cat CPU_FREQ_kHZ.txt | tr -d '[:space:]')
for F in $FOLDERS
do
    echo "cd $F"
    cd $F
    rm *.o || true
    rm $F # the executable
    make_command="make -f Makefile"
    eval $make_command
    rc=$?
    if [[ $rc != 0 ]] ; then
        echo ""
        echo "=================================== ERROR BUILDING $F - $name ===================================="
        echo ""
        exit 1
    fi
    cd ..
done

