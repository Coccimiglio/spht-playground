#!/bin/sh
FOLDERS="hashmap"

rm lib/*.o || true

for F in $FOLDERS
do
    cd $F
    rm *.o || true    
    rm *.exe
    cd ..
done
